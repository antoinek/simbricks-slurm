import argparse
import json
import os
import os.path
import shlex
import sys

parser = argparse.ArgumentParser()
parser.add_argument('--json', help='Path to the container json file')
parser.add_argument('--cmd', help='Command to run')
parser.add_argument('--mount', type=str, nargs='+', default=[], metavar='H:G',
	help='mount directory into guest')
args = parser.parse_args()

with open(args.json, 'r') as f:
	c = json.loads(f.read())

c['process']['terminal'] = False
c['process']['args'] = shlex.split(args.cmd)
c['process']['cwd'] = '/simbricks/experiments'

for k in c['process']['capabilities']:
	c['process']['capabilities'][k].append('CAP_SYS_CHROOT')
	c['process']['capabilities'][k].append('CAP_SETUID')
	c['process']['capabilities'][k].append('CAP_SETGID')

c['process']['rlimits'][0]['hard'] = 8192
c['process']['rlimits'][0]['soft'] = 8192

c['root']['path'] = os.path.dirname(os.path.abspath(args.json)) + '/rootfs'
c['root']['readonly'] = False

id_mapping = {
	'containerID': 1,
	'hostID': (os.getuid() + 1 - 1000) * 100000,
	'size': 100000
}
c['linux']['uidMappings'] = [
	# mapping for root uid
	{
		'containerID': 0,
		'hostID': os.getuid(),
		'size': 1
	},
	# add mappings for other uids
	id_mapping
]
c['linux']['gidMappings'] = [
	# mapping for root gid
	{
		'containerID': 0,
		'hostID': os.getgid(),
		'size': 1
	},
	# add mappings for other gids
	id_mapping,
	# kvm group
	{
		'containerID': 100001,
		'hostID': 105,
		'size': 1
	}
]


#del c['linux']['resources']


c['linux']['devices'] = [{
	'path': '/dev/kvm',
        'type': 'c',
        'major': 10,
        'minor': 232
}]

for i in range(0, len(c['linux']['namespaces'])):
	if c['linux']['namespaces'][i]['type'] == 'network':
		del c['linux']['namespaces'][i]
		break

for m in args.mount:
	ps = m.split(':')
	c['mounts'].append({
		'source': ps[0],
		'destination': ps[1],
		'type': 'none',
		'options': [ 'rbind', 'nosuid', 'noexec', 'nodev', 'ro' ]
	})

with open(args.json, 'w') as f:
	f.write(json.dumps(c))
