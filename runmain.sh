#!/bin/bash

bash prepcontainer.sh rootfs.tar /tmp/simbrickscontainer

python3 modify_oci.py --json=/tmp/simbrickscontainer/config.json \
	--mount=`pwd`/hosts.json:/hosts.json \
	--cmd="/usr/bin/simbricks-run --verbose --force  --hosts /hosts.json $*"

cd /tmp/simbrickscontainer
runc --root root run mycontainerid
