#!/bin/bash
#SBATCH --nodes=2 --ntasks-per-node=1
#SBATCH --cpus-per-task=5
#SBATCH -o slurm.%N.%j.out # STDOUT
#SBATCH -e slurm.%N.%j.out # STDERR

nodes="`scontrol show hostnames $SLURM_JOB_NODELIST`"
leader="`echo $nodes | awk '{print \$1}'`"
workers="`echo $nodes | awk '{for (i=2; i<=NF; i++) print \$i}'`"

if [ ! -f rootfs.tar ]; then
	docker export $(docker create simbricks/simbricks-dist-worker:latest) \
		>rootfs.tar
fi

bash make_hosts.sh >hosts.json


echo start worker containers on worker nodes
i=0
worker_pids=""
for w in $workers
do
	echo starting on $w
	srun --nodelist=$w -N1  -n1 --cpus-per-task=5 bash runworker.sh &
	worker_pids="$worker_pids $!"
	i=$(($i + 1))

done

echo Giving workers a head start
sleep 30

echo starting main
bash runmain.sh pyexps/dist_netperf.py --dist --filter=dist_netperf-qemu-i40e-1

sleep 10
kill $worker_pids
sleep 5
scancel --signal=KILL $SLURM_JOB_ID
